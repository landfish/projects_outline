# Outline of Jeff's Software Projects
A brief description of each software project I have worked on and a summary of my contributions

#### EDURange ####

https://github.com/donkey-hotei/edurange
 
 - EDURange is a cloud-based platform for teaching security to CS students. Instructors use a web interface to create scenarios of multiple VMs configured for specific exercises (such as nmap recon). The link above is to a work-in-progress fork of the project. I coded a new part of the web interface which allows instructors to save and view the bash histories of students who took part in the exercises. My objective was to make sure that bash histories were being written to the .bash_history files and then placed to an AWS bucket. Then, I worked to make that data easy to view and download. I have also been involved in planning, writing, and conceptual work in the project. We have a goal of getting EDURanage in 50 classrooms in the next 3 years. I am currently working on expanding the range of data we can collect from scenarios, such as command exit status, stdout, and stderror.

#### Ancestral Pizza ####

https://github.com/land-fish/AncestralPizza

 - I made this game as part of a class project during one academic quarter. This was my first action video game. I chose the pygame library because I like to use python and I found lots of good code examples and libraries. I succeeded in my goal of creating a simple 2D action game that was fun to play and presented the game to my class. Since I was very new to the platform, I used many pieces of sample code to get my game working, so the result is something of a frankstein monster, but a beautiful, fun-to-play monster.
 
#### KinderKrypt ####

https://github.com/tsvisimcha/KinderKrypt

- A crypto project built during a 24-hour code day sprint. Inspired by the need for user-friendly GPG programs, a couple friends and I started to build a GUI in python for GPG. I wrote the search feature as well as the help menu and files.
 
#### Tic-Tac-Toe ####

https://github.com/land-fish/TicTacToe

- The first game I created just for fun during my Java foundations class. It's a simple tic-tac-toe game that pits you against another player or the computer. There is a hard mode where the computer cannot lose and will usually win against a novice player, and an easy mode where winning is possible. I was in the midst of creating a menu interface for the game when I moved on from the project. Future features will include taunts from the AI, and a wargames-style option to have the difficult AI play itself (and learn the futility of global thermonuclear war).

#### SilverDollarGame, Blackjack, FlightPlanner ####

 https://github.com/land-fish/SilverDollarGame

 https://github.com/Philosoraptors/FlightPlanner

 https://github.com/Philosoraptors/FlightPlanner

- Projects built as part of class assignments during my Java foundations program. Since they were "collaborative" projects by novice CS students, may or may not compile. But really, I learned a lot about coding collaboratively during these projects (and the importance of choosing good team members).

